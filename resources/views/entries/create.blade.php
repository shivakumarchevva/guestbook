@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-sm-8 offset-sm-2">
      <h1>Add a entry</h1>
      <div>
        @if ($errors->any())
          <div class="alert alert-danger">
            <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
          </div><br />
        @endif
        <form method="post" action="{{ route('entries.store') }}">
        @csrf
          <div class="form-group">
            <label for="name">Name:</label>
            <input type="text" class="form-control" name="name"/>
          </div>

          <div class="form-group">
            <label for="entry">Entry:</label>
            <textarea class="form-control" name="entry" id="entry" rows="3"></textarea>
          </div>

          <div class="form-group">
            <label for="comment">Comment:</label>
            <textarea class="form-control" name="comment" id="comment" rows="3"></textarea>
          </div>

          <button type="submit" class="btn btn-primary-outline">Add entry</button>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection