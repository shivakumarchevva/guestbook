@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-sm-12">
      @if(session()->get('success'))
        <div class="alert alert-success">
          {{ session()->get('success') }}
        </div>
      @endif
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12">
      <h1>Guest Entries</h1>
	  @auth
      <div>
        <a href="{{ route('entries.create')}}" class="btn btn-primary">New entry</a>
      </div>
      <table class="table table-striped">
        <thead>
          <tr>
            <td>ID</td>
            <td>Name</td>
            <td>Entry</td>
            <td>Comment</td>
            <td>Status</td>
            <td colspan = 4>Actions</td>
          </tr>
        </thead>
        <tbody>
          @foreach($entries as $entry)
          @can('view', $entry)
          <tr>
            <td>{{$entry->id}}</td>
            <td>{{$entry->name}} {{$entry->last_name}}</td>
            <td>{{$entry->entry}}</td>
            <td>{{$entry->comment}}</td>
            <td>{{$entry->status->label}}</td>
			@can('delete', $entry)
			@if($entry->status->label == 'Draft')
            <td>
              <a href="{{ route('entries.edit',$entry->id)}}" class="btn btn-primary">Edit</a>
            </td>
            <td>
              <form action="{{ route('entries.approve', $entry->id)}}" method="post">
                @csrf
                @method('PATCH')
                <button class="btn btn-primary" type="submit">Approve</button>
              </form>
            </td>
            <td>
              <form action="{{ route('entries.reject', $entry->id)}}" method="post">
                @csrf
                @method('PATCH')
                <button class="btn btn-primary" type="submit">Reject</button>
              </form>
            </td>
            <td>
              <form action="{{ route('entries.destroy', $entry->id)}}" method="post">
                @csrf
                @method('DELETE')
                <button class="btn btn-danger" type="submit">Delete</button>
              </form>
            </td>
			@endif
            @endcan
          </tr>
          @endcan
          @endforeach
        </tbody>
      </table>
	  @endauth
    <div>
  </div>
</div>
@endsection