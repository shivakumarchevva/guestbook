@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-sm-8 offset-sm-2">
      <h1>Welcome to Guestbook</h1>
      <div>
        @auth
        <a href="{{ route('entries.index')}}" class="btn btn-primary">Go to Entries</a>
        @endauth
      </div>
    </div>
  </div>
</div>
@endsection
