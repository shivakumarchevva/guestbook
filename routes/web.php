<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Route::patch('entries/{entry}/approve', 'EntryController@approve')->name('entries.approve');
Route::patch('entries/{entry}/reject', 'EntryController@reject')->name('entries.reject');

Route::resource('entries', 'EntryController');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
