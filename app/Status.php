<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    CONST STATUS_APPROVE = 'approve';
    CONST STATUS_REJECT = 'reject';
    CONST STATUS_DRAFT = 'draft';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'label'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'string',
    ];

    //public $incrementing = false;

    public function entries()
    {
        return $this->hasMany('App\Entry', 'status_id', 'id');
    }
}
