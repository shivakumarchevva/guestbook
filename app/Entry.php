<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Entry extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'entry', 'comment', 'user_id', 'status_id'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'status_id' => 'string',
    ];

    public function author() {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function status() {
        return $this->belongsTo('App\Status', 'status_id', 'id');
    }
}
