<?php

namespace App\Http\Controllers;

use App\Entry;
use App\Status;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EntryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('create', Entry::class);
        $entries = Entry::all();
        return view('entries.index', compact('entries'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', Entry::class);
        return view('entries.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'entry'=>'required',
            'comment'=>'required'
        ]);

        $entry = new Entry([
            'name' => $request->get('name'),
            'entry' => $request->get('entry'),
            'comment' => $request->get('comment'),
            'user_id' => Auth::user()->id,
            'status_id' => Status::STATUS_DRAFT
        ]);
        $entry->save();
        return redirect('/entries')->with('success', 'Entry saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $entry = Entry::find($id);
        $this->authorize('update', $entry);
        return view('entries.edit', compact('entry'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'=>'required',
            'entry'=>'required',
            'comment'=>'required'
        ]);

        $contact = Entry::find($id);
        $contact->name =  $request->get('name');
        $contact->entry = $request->get('entry');
        $contact->comment = $request->get('comment');
        $contact->save();

        return redirect('/entries')->with('success', 'Entry updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $entry = Entry::find($id);
        $this->authorize('delete', $entry);
        $entry->delete();

        return redirect('/entries')->with('success', 'Entry deleted!');
    }

    /**
     * Approve the Entry
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function approve($id) {
        $entry = Entry::find($id);

        $this->authorize('update', $entry);
        $entry->status()->associate(Status::STATUS_APPROVE);
        $entry->save();
        return redirect('/entries')->with('success', 'Entry approved!');
    }

    /**
     * Reject the Entry
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function reject($id) {
        $entry = Entry::find($id);

        $this->authorize('update', $entry);
        $entry->status()->associate(Status::STATUS_REJECT);
        $entry->save();
        return redirect('/entries')->with('success', 'Entry rejected!');
    }
}
