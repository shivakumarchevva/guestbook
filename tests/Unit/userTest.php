<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Hash;

class userTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testUserIsCreated()
    {
        $user = factory(User::class)->create([
            'name' => 'test',
            'email' => 'test@example.com',
            'password' => Hash::make('password')
        ]);
        $this->assertEquals('test', $user->name);
    }
}
