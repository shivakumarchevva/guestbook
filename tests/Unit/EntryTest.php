<?php

namespace Tests\Unit;

use App\Entry;
use App\Status;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class EntryTest extends TestCase
{
    use DatabaseTransactions;
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testGuestCanCreateEntry()
    {
        $user = factory(User::class)->create();
        $entry = Entry::create([
            'name' => 'Test name',
            'entry' => 'Test entry',
            'comment' => 'Test comment',
            'user_id' => $user->id,
            'status_id' => Status::STATUS_DRAFT
        ]);
        $this->assertEquals('Test name', $entry->name);

    }
}
