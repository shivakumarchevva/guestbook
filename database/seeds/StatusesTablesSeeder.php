<?php

use Illuminate\Database\Seeder;

class StatusesTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Status::create([
            'id' => App\Status::STATUS_DRAFT,
            'label' => 'Draft'
        ]);
        App\Status::create([
            'id' => App\Status::STATUS_APPROVE,
            'label' => 'Approve'
        ]);
        App\Status::create([
            'id' => App\Status::STATUS_REJECT,
            'label' => 'Reject'
        ]);
    }
}
