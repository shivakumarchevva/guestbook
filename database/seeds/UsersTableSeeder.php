<?php

use App\Role;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = \App\User::create([
            'name' => 'admin',
            'email' => 'admin@example.com',
            'password' => Hash::make('password')
        ]);
        $user->roles()->save(\App\Role::find(\App\Role::ADMIN));

        //Create guest
        $user = App\User::create([
            'name' => 'guest',
            'email' => 'guest@example.com',
            'password' => Hash::make('password')
        ]);
        $user->roles()->save(App\Role::find(App\Role::GUEST));
    }
}
