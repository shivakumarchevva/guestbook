<?php

use App\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Create Admin role
        DB::table('roles')->insert([
            'id' => Role::ADMIN,
            'label' => 'Administrator',
        ]);
        //Create Guest role
        DB::table('roles')->insert([
            'id' => Role::GUEST,
            'label' => 'Guest',
        ]);
    }
}
