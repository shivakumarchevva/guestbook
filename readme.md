## Instructions
- composer intall
- copy .env.example to .env and change database settings
- php artisan migrate
- php artisan db:seed
- npm install
- `npm run prod` to build and `npm run watch` to compile assets.
- Run `vendor/bin/phpunit` to run tests

## Default users
- Admin user: admin@example.com/password
- Guest user: guest@example.com/password

Login with above credentails and test the App!

Testing Feature branch
